function IsExistsSolutionNumberInbrand() {
    var txtSolutionNumber = $("#txtSolutionNumber");
    if (txtSolutionNumber.val().trim() != "") {
        txtSolutionNumber.removeAttr("required");
        txtSolutionNumber.removeAttr("title");
        txtSolutionNumber.removeClass("invalidSolutionNumber");
        $("#lblSolutionNumber").remove();
        $.ajax({
            type: "POST",
            url: "SimpleSignUp.aspx/isExistsSolutionNumberInbrand",
            data: '{strSolutionNumber: "' + txtSolutionNumber.val().trim() + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                ShowLoadingPanel();
            },
            success: function (response) {
                if (response.d.isExists == false) {
                    txtSolutionNumber.removeAttr("required");
                    txtSolutionNumber.removeClass("invalidSolutionNumber");
                    $("#lblSolutionNumber").remove();
                }
                else {
                    txtSolutionNumber.attr("required", true);
                    txtSolutionNumber.addClass("invalidSolutionNumber");
                    $("#lblSolutionNumber").remove();
                    var isInValid = $("#txtSolutionNumber").hasClass("invalidSolutionNumber");
                    if (isInValid) {
                        $("<label id='lblSolutionNumber' for='txtSolutionNumber' class='errorLable' generated='true' >Solution Number Taken</label>").insertBefore(".invalidSolutionNumber");
                    }
                    setBorderColor();
                }
                hideLoadingPanel();
            },
            failure: function (response) {
                hideLoadingPanel();
                alert(response.d);
            }
        });
    }
    else {
        txtSolutionNumber.attr("required", true);
        txtSolutionNumber.addClass("invalidSolutionNumber");
        $("#lblSolutionNumber").remove();
    }
}
